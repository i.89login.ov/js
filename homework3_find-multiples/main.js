inputValue();

function inputValue() {
  let userAnswer = prompt("Введи число", "");

  if (isNaN(userAnswer) || !userAnswer) {
    return inputValue();
  }

  if (userAnswer < 5) {
    console.log("Sorry, no numbers");
  } else {
    for (let i = 0; i <= userAnswer; i++) {
      if (i % 5 === 0) {
        console.log(i);
      }
    }
  }
}

//! доп дз

// let m = +prompt("Enter the smaller number", "");
// let n = +prompt("Enter a larger number", "");
//
// while (m > n) {
//   alert("Error, the first number must be less than the second");
//
//   m = +prompt("Enter the smaller number", "");
//   n = +prompt("Enter a larger number", "");
// }
//
// simple: for (let i = 2; i <= n; i++) {
//   if (i < m) continue;
//   for (let j = 2; j < i; j++) {
//     if (i % j === 0) continue simple;
//   }
//   console.log(i);
// }
