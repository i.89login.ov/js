'use strict';
const image = document.querySelectorAll('.image-to-show');

const btnStop = document.createElement('button');
btnStop.textContent = 'Припинити';
btnStop.classList.add('stop');
document.body.append(btnStop);

const btnPlay = document.createElement('button');
btnPlay.textContent = 'Відновити показ';
btnPlay.classList.add('play');
document.body.append(btnPlay);

const stop = document.querySelector('.stop');
const play = document.querySelector('.play');

let COUNT = 0;
let interval;

function sliderImage() {
	interval = setInterval(() => {
		if (COUNT === image.length - 1) {
			image[COUNT].classList.toggle('show');
			COUNT = -1;
			image[COUNT + 1].classList.toggle('show');
		} else {
			image[COUNT].classList.toggle('show');
			image[COUNT + 1].classList.toggle('show');
		}
		COUNT++;
	}, 3000);
}

sliderImage();

stop.addEventListener('click', e => {
	clearInterval(interval);
});

play.addEventListener('click', e => {
	sliderImage();
});
