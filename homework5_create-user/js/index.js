const createNewUser = () => {
	const firstName = prompt('Введи имя', '').trim();
	const lastName = prompt('Введи фамилию', '').trim();
	const newUser = {};

	Object.defineProperties(newUser, {
		firstName: { value: firstName, configurable: true },
		lastName: { value: lastName, configurable: true },
	});

	newUser.getLogin = function () {
		return this.firstName.trim()[0].toLowerCase() + this.lastName.toLowerCase();
	};

	newUser.setFirstName = function (firsNameNew) {
		Object.defineProperty(newUser, 'firstName', {
			value: firsNameNew,
		});
	};

	newUser.setLastName = function (lastNameNew) {
		Object.defineProperty(newUser, 'lastName', {
			value: lastNameNew,
		});
	};

	return newUser;
};

let userNew = createNewUser();

console.log(userNew.getLogin());
