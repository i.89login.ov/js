const createNewUser = () => {
	const firstName = prompt('Введи имя', '').trim();
	const lastName = prompt('Введи фамилию', '').trim();
	const birthday = prompt('Введите дату в формате dd.mm.yyyy', '').trim();
	const newUser = {};

	Object.defineProperties(newUser, {
		firstName: { value: firstName, configurable: true },
		lastName: { value: lastName, configurable: true },
		birthday: { value: birthday, configurable: true },
	});

	newUser.getLogin = function () {
		return this.firstName.trim()[0].toLowerCase() + this.lastName.toLowerCase();
	};

	newUser.getAge = function () {
		const convertingBd = () => {
			const [day, month, year] = this.birthday.split('.');
			return new Date([month, day, year].join('.'));
		};
		const birthday = convertingBd();
		return new Date(Date.now() - birthday).getFullYear() - new Date(0).getFullYear();
	};

	newUser.getPassword = function () {
		const year = this.birthday.slice(6, 10);
		return this.firstName.trim()[0].toUpperCase() + this.lastName.toLowerCase() + year;
	};

	newUser.setFirstName = function (firsNameNew) {
		Object.defineProperty(newUser, 'firstName', {
			value: firsNameNew,
		});
	};

	newUser.setLastName = function (lastNameNew) {
		Object.defineProperty(newUser, 'lastName', {
			value: lastNameNew,
		});
	};

	return newUser;
};

let userNew = createNewUser();

console.log(userNew.getLogin());
console.log(userNew.getAge());
console.log(userNew.getPassword());
