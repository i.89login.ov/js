const i = document.querySelectorAll('.show');
const firstPassword = document.querySelector('#input1');
const secondPassword = document.querySelector('#input2');
const error = document.querySelector('.error');
const button = document.querySelector('.btn');

i.forEach(function (eye) {
	eye.onclick = function () {
		let target = this.getAttribute('data-target'),
			inputPass = document.querySelector(target);
		if (inputPass.getAttribute('type') === 'password') {
			inputPass.setAttribute('type', 'text');
			eye.classList.toggle('fa-eye-slash');
		} else {
			inputPass.setAttribute('type', 'password');
			eye.classList.toggle('fa-eye-slash');
		}
	};
});

button.addEventListener('click', function (btn) {
	btn.preventDefault();
	if (firstPassword.value !== secondPassword.value) {
		error.innerHTML = 'Должны быть одинаковые пароли';
	} else if (firstPassword.value.length < 8 && secondPassword.value.length < 8) {
		error.innerHTML = 'Ваш пароль должен иметь не меньше 8 символов';
	} else if (firstPassword.value.search(/[a-z]/) < 0 && secondPassword.value.search(/[a-z]/) < 0) {
		error.innerHTML = 'должна быть одна маленькая буква';
	} else if (firstPassword.value.search(/[A-Z]/) < 0 && secondPassword.value.search(/[A-Z]/) < 0) {
		error.innerHTML = 'должна быть одна большая буква';
	} else if (firstPassword.value.search(/[0-9]/) < 0 && secondPassword.value.search(/[0-9]/) < 0) {
		error.innerHTML = 'должна быть хотя бы одна цифра';
	} else if (firstPassword.value.includes(' ') && secondPassword.value.includes(' ')) {
		error.innerHTML = 'пароль не должен иметь пробел';
	} else {
		alert('You are welcome!');
	}
});
