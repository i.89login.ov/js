//! Рекурсия - это функция которая вызывает саму себя

const number = +prompt('Ввведи число', '');

while (isNaN(number)) {
	+prompt('Ввведи число', '');
}
console.log(factorial(number));

function factorial(num) {
	if (num < 0) {
		console.log('Error');
	} else if (num === 0) {
		return 1;
	} else {
		return num * factorial(num - 1);
	}
}
