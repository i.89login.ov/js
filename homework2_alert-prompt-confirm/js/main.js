for (let i = 0; i < 1; i += 1) {
  const name = prompt("What is your name?", "");
  const age = +prompt("How old are you?");

  if (name !== null && age !== undefined && name !== "" && !isNaN(age)) {
    if (age < 18) {
      alert("You are not allowed to visit this website");
    } else if (age >= 18 && age <= 22) {
      if (confirm("Are you sure you want to continue?") === true) {
        alert(`Welcome ${name}`);
      } else {
        alert("You are not allowed to visit this website");
      }
    } else {
      alert(`Welcome ${name}`);
    }
  } else {
    i -= 1;
  }
}
