const tab = function () {
	const tabMenu = document.querySelectorAll('.tabs-title');
	const tabContent = document.querySelectorAll('.tab');
	let tabName;

	tabMenu.forEach(item => {
		item.addEventListener('click', selectTabMenu);
	});

	function selectTabMenu() {
		tabMenu.forEach(item => {
			item.classList.remove('active');
		});

		this.classList.add('active');
		tabName = this.getAttribute('data-tab-name');
		selectTabContent(tabName);
	}

	function selectTabContent(tabName) {
		tabContent.forEach(item => {
			item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
		});
	}
};

tab();
