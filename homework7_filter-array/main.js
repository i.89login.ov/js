//! filter

const filterBy = (array, type) => array.filter(item => typeof item !== type);

console.log(filterBy(['hello', 'world', 23, '23', null, 72, 'ilya', NaN], 'string'));

//! reduce

function filterReduce(array, type) {
	return array.reduce((res, currentItem) => {
		if (type !== typeof currentItem) {
			res.push(currentItem);
		}
		return res;
	}, []);
}
console.log(filterReduce(['hello', 'world', 23, '23', null, 72, 'ilya', NaN], 'string'));

//! forEach

function fil(arr, type) {
	const array1 = [];
	arr.forEach(item => {
		if (type !== typeof item) {
			array1.push(item);
		}
	});
	console.log(array1);
}
fil(['hello', 'world', 23, '23', null, 72, 'ilya', NaN], 'string');
