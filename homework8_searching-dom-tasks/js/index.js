const paragraph = document.querySelectorAll('p');
const options = document.querySelector('#optionsList');
const testParagraph = document.querySelector('#testParagraph');
const mainHeader = document.querySelector('.main-header');
const sectionTitle = document.querySelectorAll('.section-title');

// -----1-----

paragraph.forEach(item => (item.style.backgroundColor = '#ff0000'));

// -----2-----

console.log(options);
console.log(options.parentNode);

const child = options.childNodes;
console.log(child);

if (child) {
	for (const ch of child) {
		console.log(`Название ch-${ch.nodeName}`);
		console.log(`Тип ch-${ch.nodeType}`);
	}
}

// -----3-----

testParagraph.textContent = 'This is a paragraph';

// -----4-----

console.log(mainHeader.children);

for (const main of mainHeader.children) {
	main.classList.add('nav-item');
}

// -----5-----

sectionTitle.forEach(item => item.classList.remove('section-title'));
