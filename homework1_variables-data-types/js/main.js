// *Объявите две переменные: admin и name. Установите значение переменной name в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.

let admin;
const name = "Ilya";

admin = name;

console.log(admin);

// *Объявите переменную days и проинициализируйте ее числом от 1 до 10. Преобразуйте это число в количество секунд и выведите в консоль.

const days = Math.round(Math.random() * 10);
const sec = days * 24 * 60 * 60;

console.log(days);
console.log(sec);

// *Запросите у пользователя какое либо значение и выведите его в консоль.

const youName = prompt("Как тебя зовут?");
console.log(youName);

const isOver18 = confirm("Тебе есть 18?");
console.log(isOver18);
