`use strict`;
const array = ['Kharkiv', 'Kiev', ['Borispol', 'Irpin'], 'Odessa', 'Lviv', 'Dnieper'];

const arrList = (arr, parent = 'body') => {
	const parentElement = document.querySelector(parent);
	const ul = document.createElement('ul');

	parentElement.append(ul);

	arr.map(value => {
		if (!Array.isArray(value)) {
			ul.insertAdjacentHTML('beforeend', `<li>${value}</li>`);
		} else {
			const list = arrList(value);
			ul.append(list);
		}
	});

	return ul;
};

arrList(array);
